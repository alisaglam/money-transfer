package com.revolut.repository

import com.revolut.entity.Account
import javax.inject.Singleton

@Singleton
class InMemoryAccountRepository : AccountRepository {
    private val accountMap = HashMap<String, Account>()

    override fun save(account: Account): Account {
        accountMap[account.accountNumber] = account
        return account
    }

    override fun getByAccountNumber(accountNumber: String): Account? {
        return accountMap[accountNumber]
    }

    override fun getAll(): List<Account> {
        return accountMap.values.toList()
    }
}
