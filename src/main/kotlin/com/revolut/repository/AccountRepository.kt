package com.revolut.repository

import com.revolut.entity.Account

interface AccountRepository {
    fun save(account: Account): Account
    fun getByAccountNumber(accountNumber: String): Account?
    fun getAll(): List<Account>
}
