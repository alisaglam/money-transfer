package com.revolut.entity

data class Account(
        val accountNumber: String,
        val accountHolder: String,
        val currency: Currency,
        val balance: Money
)
