package com.revolut.entity

enum class Currency(
    val currencyCode: String,
    val centFactor: Int
) {
    GBP("GBP", 100),
    EUR("EUR", 100),
    USD("USD", 100);
}
