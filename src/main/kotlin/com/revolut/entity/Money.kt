package com.revolut.entity

import java.math.BigDecimal
import java.math.RoundingMode

class Money : Comparable<Money> {
    var amount: BigDecimal
    var currency = Currency.GBP

    val isZeroAmount: Boolean
        get() = Integer.valueOf(0) == this.amount.toInt()

    constructor() {
        amount = BigDecimal(0)
    }

    private constructor(amount: BigDecimal, currency: Currency) {
        this.currency = currency
        this.amount = amount
    }

    private fun newMoney(amount: BigDecimal, roundingMode: RoundingMode = DEFAULT_ROUNDING_MODE): Money {
        return Money(amount.setScale(calculateScale(this.currency), roundingMode), this.currency)
    }

    private fun newMoney(
            amount: BigDecimal,
            currency: Currency,
            roundingMode: RoundingMode = DEFAULT_ROUNDING_MODE
    ): Money {
        return Money(amount.setScale(calculateScale(currency), roundingMode), currency)
    }

    fun add(addedMoney: Money): Money {
        assertSameCurrencyAs(addedMoney)
        return newMoney(amount.add(addedMoney.amount))
    }

    fun substract(subtractedMoney: Money): Money {
        assertSameCurrencyAs(subtractedMoney)
        return newMoney(amount.subtract(subtractedMoney.amount))
    }

    private fun assertSameCurrencyAs(other: Money) {
        if (other.currency != this.currency) {
            throw RuntimeException("Currency mismatch!")
        }
    }

    override fun hashCode(): Int {
        return amount.hashCode() + currency.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null) {
            return false
        }

        return if (other is Money) {
            currency == other.currency && compareTo(other) == 0
        } else false
    }

    override fun compareTo(other: Money): Int {
        assertSameCurrencyAs(other)
        return amount.compareTo(other.amount)
    }

    fun greaterThan(other: Money): Boolean {
        assertSameCurrencyAs(other)
        return compareTo(other) > 0
    }

    fun greaterThanOrEqual(other: Money): Boolean {
        assertSameCurrencyAs(other)
        return compareTo(other) >= 0
    }

    fun lessThan(other: Money): Boolean {
        assertSameCurrencyAs(other)
        return compareTo(other) < 0
    }

    fun lessThanOrEqual(other: Money): Boolean {
        assertSameCurrencyAs(other)
        return compareTo(other) <= 0
    }

    companion object {
        val DEFAULT_ROUNDING_MODE = RoundingMode.HALF_UP

        fun createMoney(amount: Double, currency: Currency): Money {
            assertFractionalPartAllowed(amount, currency)
            return Money(BigDecimal(amount).setScale(calculateScale(currency), DEFAULT_ROUNDING_MODE), currency)
        }

        fun calculateScale(currency: Currency) = Math.log10(currency.centFactor.toDouble()).toInt()

        fun gbp(amount: Double): Money {
            return createMoney(amount, Currency.GBP)
        }

        fun usd(amount: Double): Money {
            return createMoney(amount, Currency.USD)
        }

        private fun assertFractionalPartAllowed(amount: Double, currency: Currency) {
            val fractionalPartExisting = amount % 1 > 0
            if (fractionalPartExisting && currency.centFactor == 1) {
                throw IllegalArgumentException("Fractional part is not allowed for this currency")
            }
        }
    }
}
