package com.revolut.service.exception

import com.revolut.common.exception.BusinessRuleException

class InvalidTransferAmountException : BusinessRuleException("invalid.transfer.amount")
