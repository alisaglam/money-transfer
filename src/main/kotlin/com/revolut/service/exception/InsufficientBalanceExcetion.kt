package com.revolut.service.exception

import com.revolut.common.exception.BusinessRuleException

class InsufficientBalanceExcetion : BusinessRuleException("insufficient.balance")
