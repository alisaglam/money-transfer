package com.revolut.service.exception

import com.revolut.common.exception.BusinessRuleException

class AccountCurrencyMismatchException : BusinessRuleException("account.currency.mismatch")
