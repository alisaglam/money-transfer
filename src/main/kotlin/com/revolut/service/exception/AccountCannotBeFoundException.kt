package com.revolut.service.exception

import com.revolut.common.exception.BusinessRuleException

class AccountCannotBeFoundException(accountNumber: String) : BusinessRuleException("account.not.found", accountNumber)
