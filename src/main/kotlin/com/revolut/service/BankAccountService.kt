package com.revolut.service

import com.revolut.controller.payload.AddAmountPayload
import com.revolut.controller.payload.CreateBankAccountPayload
import com.revolut.controller.payload.TransferMoneyPayload
import com.revolut.entity.Account
import com.revolut.entity.Money
import com.revolut.repository.AccountRepository
import com.revolut.repository.InMemoryAccountRepository
import com.revolut.service.exception.*
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class BankAccountService @Inject constructor(private val repository: InMemoryAccountRepository) : AccountRepository by repository {

    fun transferMoney(transferMoneyPayload: TransferMoneyPayload): Account {
        checkAccountNumbersAreNotSame(transferMoneyPayload)
        var sourceAccount = repository.getByAccountNumber(transferMoneyPayload.sourceAccountNumber)
                ?: throw AccountCannotBeFoundException(transferMoneyPayload.sourceAccountNumber)
        var targetAccount = repository.getByAccountNumber(transferMoneyPayload.targetAccountNumber)
                ?: throw AccountCannotBeFoundException(transferMoneyPayload.targetAccountNumber)
        val transferredAmount = transferMoneyPayload.amount.money
        checkAccountCurrencyIsMatchingWithTheAmount(sourceAccount, transferredAmount)
        checkAccountCurrencyIsMatchingWithTheAmount(targetAccount, transferredAmount)
        checkAmountIsNotZero(transferredAmount)

        // lock always in the same order for 2 accounts so that it will not cause dead-locks
        val (lock1, lock2) = getLockObjectsInOrdered(sourceAccount.accountNumber, targetAccount.accountNumber)
        synchronized(lock1) {
            synchronized(lock2) {
                sourceAccount = repository.getByAccountNumber(transferMoneyPayload.sourceAccountNumber)!!
                targetAccount = repository.getByAccountNumber(transferMoneyPayload.targetAccountNumber)!!

                val updatedSourceAccount = withdrawAmount(sourceAccount, transferredAmount)
                addAmount(targetAccount, transferredAmount)
                return updatedSourceAccount
            }
        }
    }

    private fun checkAccountNumbersAreNotSame(transferMoneyPayload: TransferMoneyPayload) {
        if (transferMoneyPayload.sourceAccountNumber == transferMoneyPayload.targetAccountNumber) {
            throw CannotTransferMoneyToSameAccountNumberException()
        }
    }

    private fun getLockObjectsInOrdered(sourceAccountNumber: String, targetAccountNumber: String): Pair<String, String> {
        val lock1 = if (sourceAccountNumber.hashCode() < targetAccountNumber.hashCode()) sourceAccountNumber else targetAccountNumber
        val lock2 = if (lock1 == sourceAccountNumber) targetAccountNumber else sourceAccountNumber
        return Pair(lock1, lock2)
    }

    fun addAmountToAccount(addAmountPayload: AddAmountPayload): Account {
        var account = repository.getByAccountNumber(addAmountPayload.accountNumber)
                ?: throw AccountCannotBeFoundException(addAmountPayload.accountNumber)
        val toBeAddedAmount = addAmountPayload.amount.money
        checkAccountCurrencyIsMatchingWithTheAmount(account, toBeAddedAmount)
        checkAmountIsNotZero(toBeAddedAmount)
        synchronized(account.accountNumber) {
            account = repository.getByAccountNumber(addAmountPayload.accountNumber)!!
            return addAmount(account, toBeAddedAmount)
        }
    }

    private fun checkAmountIsNotZero(toBeAddedAmount: Money) {
        if (toBeAddedAmount.isZeroAmount) {
            throw InvalidTransferAmountException()
        }
    }

    private fun checkAccountCurrencyIsMatchingWithTheAmount(account: Account, amount: Money) {
        if (account.currency != amount.currency) {
            throw AccountCurrencyMismatchException()
        }
    }

    private fun addAmount(account: Account, amount: Money): Account {
        return repository.save(account.copy(balance = account.balance.add(amount)))
    }

    private fun withdrawAmount(account: Account, amount: Money): Account {
        if (account.balance.lessThan(amount)) {
            throw InsufficientBalanceExcetion()
        }
        val updatedAmount = account.copy(balance = account.balance.substract(amount))
        return repository.save(updatedAmount)
    }

    fun createAccount(createBankAccountPayload: CreateBankAccountPayload): Account {
        val account = Account(UUID.randomUUID().toString(),
                createBankAccountPayload.accountHolder,
                createBankAccountPayload.currency,
                Money.createMoney(0.0, createBankAccountPayload.currency))
        return repository.save(account)
    }
}
