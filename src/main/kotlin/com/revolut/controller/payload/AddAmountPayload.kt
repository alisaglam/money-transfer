package com.revolut.controller.payload

import com.revolut.common.payload.MoneyPayload
import com.revolut.common.payload.RequestPayload
import javax.validation.Valid
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class AddAmountPayload(
        @field:NotEmpty val accountNumber: String,
        @field:NotNull @get:Valid val amount: MoneyPayload
) : RequestPayload
