package com.revolut.controller.payload

import com.revolut.common.payload.MoneyPayload
import com.revolut.common.payload.RequestPayload
import javax.validation.Valid
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class TransferMoneyPayload(
        @field:NotEmpty val sourceAccountNumber: String,
        @field:NotEmpty val targetAccountNumber: String,
        @field:NotNull @get:Valid val amount: MoneyPayload
) : RequestPayload
