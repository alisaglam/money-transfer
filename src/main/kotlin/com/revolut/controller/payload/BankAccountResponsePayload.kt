package com.revolut.controller.payload

import com.revolut.common.payload.MoneyPayload
import com.revolut.common.payload.ResponsePayload
import com.revolut.entity.Account

data class BankAccountResponsePayload(
        val accountNumber: String,
        val accountHolder: String,
        val balance: MoneyPayload
) : ResponsePayload {
    constructor(account: Account) : this(account.accountNumber, account.accountHolder, MoneyPayload(account.balance))
}
