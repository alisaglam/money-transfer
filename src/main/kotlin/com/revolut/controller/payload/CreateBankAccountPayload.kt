package com.revolut.controller.payload

import com.revolut.common.payload.RequestPayload
import com.revolut.entity.Currency
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class CreateBankAccountPayload(
        @field:NotEmpty val accountHolder: String,
        @field:NotNull val currency: Currency
) : RequestPayload
