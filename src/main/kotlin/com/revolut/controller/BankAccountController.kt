package com.revolut.controller

import com.revolut.controller.payload.AddAmountPayload
import com.revolut.controller.payload.BankAccountResponsePayload
import com.revolut.controller.payload.CreateBankAccountPayload
import com.revolut.controller.payload.TransferMoneyPayload
import com.revolut.service.BankAccountService
import com.revolut.service.exception.AccountCannotBeFoundException
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*
import io.micronaut.validation.Validated
import java.net.URI
import javax.inject.Inject
import javax.validation.Valid

@Controller("/bank-accounts")
@Produces(MediaType.APPLICATION_JSON)
@Validated
class BankAccountController @Inject constructor(private val service: BankAccountService) {

    @Post
    fun create(@Body @Valid createBankAccountPayload: CreateBankAccountPayload, request: HttpRequest<*>): HttpResponse<BankAccountResponsePayload>? {
        val account = service.createAccount(createBankAccountPayload)
        return HttpResponse.created(BankAccountResponsePayload(account), URI.create("${request.uri}/${account.accountNumber}"))
    }

    @Get("/{accountNumber}")
    fun get(@PathVariable accountNumber: String): BankAccountResponsePayload {
        val account = service.getByAccountNumber(accountNumber) ?: throw AccountCannotBeFoundException(accountNumber)
        return BankAccountResponsePayload(account)
    }

    @Get
    fun list(): List<BankAccountResponsePayload> {
        val allAccounts = service.getAll()
        return allAccounts.map { BankAccountResponsePayload(it) }
    }

    @Post("transfer-money")
    fun transferMoney(@Body @Valid transferMoneyPayload: TransferMoneyPayload): BankAccountResponsePayload {
        val account = service.transferMoney(transferMoneyPayload)
        return BankAccountResponsePayload(account)
    }

    @Post("add-amount")
    fun addAmount(@Body @Valid addAmountPayload: AddAmountPayload): BankAccountResponsePayload {
        val account = service.addAmountToAccount(addAmountPayload)
        return BankAccountResponsePayload(account)
    }
}
