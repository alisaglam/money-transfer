package com.revolut

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("com.revolut")
                .mainClass(Application.javaClass)
                .start()
    }
}
