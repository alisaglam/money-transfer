package com.revolut.common.exception

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.annotation.Produces
import io.micronaut.http.exceptions.HttpStatusException
import io.micronaut.http.server.exceptions.ExceptionHandler
import java.text.MessageFormat
import java.util.*
import javax.inject.Singleton


@Produces
@Singleton
class BusinessRuleExceptionHandler : ExceptionHandler<BusinessRuleException, HttpResponse<Any>> {
    override fun handle(request: HttpRequest<*>, exception: BusinessRuleException): HttpResponse<Any> {
        val resourceBundle = ResourceBundle.getBundle("messages", request.locale.orElse(Locale.getDefault()))
        throw HttpStatusException(HttpStatus.UNPROCESSABLE_ENTITY, MessageFormat.format(resourceBundle.getString(exception.messageCode),
                exception.parameters.map { it }))
    }
}
