package com.revolut.common.exception

abstract class BusinessRuleException(val messageCode: String, vararg val parameters: String) : Exception()
