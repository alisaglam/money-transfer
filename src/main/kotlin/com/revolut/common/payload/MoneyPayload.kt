package com.revolut.common.payload

import com.fasterxml.jackson.annotation.JsonIgnore
import com.revolut.entity.Currency
import com.revolut.entity.Money
import javax.validation.constraints.DecimalMin
import javax.validation.constraints.NotNull

data class MoneyPayload(
        @field:DecimalMin("0.0") val amount: Double,
        @field:NotNull val currency: Currency
) : RequestPayload, ResponsePayload {
    constructor(money: Money) : this(money.amount.toDouble(), money.currency)

    @get:JsonIgnore
    val money: Money
        get() {
            return Money.createMoney(amount, currency)
        }
}
