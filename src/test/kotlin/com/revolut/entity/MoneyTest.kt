package com.revolut.entity

import org.junit.jupiter.api.Assertions.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertFailsWith

object MoneyTest: Spek({
    describe("Money Suite") {
        it("should add two money amounts") {
            val money1 = Money.usd(1573.65)
            val money2 = Money.usd(57878.78)
            val result = money1.add(money2)
            assertEquals(59452.43, result.amount.toDouble())
            assertEquals(Currency.USD, result.currency)
        }

        it("should give error for two money amounts which have different currencies on add operation") {
            val money1 = Money.usd(1573.65)
            val money2 = Money.gbp(57878.78)
            assertFailsWith(RuntimeException::class){ money1.add(money2) }
        }

        it("should subtract amount from money") {
            val money1 = Money.usd(1573.65)
            val money2 = Money.usd(73.65)
            val result = money1.substract(money2)
            assertEquals(1500.00, result.amount.toDouble())
            assertEquals(Currency.USD, result.currency)
        }

        it("give error for two money amounts which have different currencies on subtract operation") {
            val money1 = Money.usd(1573.65)
            val money2 = Money.gbp(73.65)
            assertFailsWith(RuntimeException::class){ money1.substract(money2) }
        }

        it("should compare money objects") {
            val money1 = Money.usd(1573.65)
            val money2 = Money.usd(57878.78)
            val money3 = Money.usd(57878.78)
            assertFalse(money1.isZeroAmount)
            assertTrue(money2.greaterThan(money1))
            assertFalse(money2.greaterThan(money2))
            assertTrue(money2.greaterThanOrEqual(money3))
            assertTrue(money1.lessThan(money2))
            assertFalse(money2.lessThan(money3))
            assertTrue(money2.lessThanOrEqual(money3))
        }
    }
})
