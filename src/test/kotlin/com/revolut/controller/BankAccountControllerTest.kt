package com.revolut.controller

import com.revolut.common.payload.MoneyPayload
import com.revolut.controller.payload.AddAmountPayload
import com.revolut.controller.payload.BankAccountResponsePayload
import com.revolut.controller.payload.CreateBankAccountPayload
import com.revolut.controller.payload.TransferMoneyPayload
import com.revolut.entity.Currency
import com.revolut.entity.Money
import io.micronaut.context.ApplicationContext
import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer
import org.junit.jupiter.api.Assertions.assertEquals
import org.slf4j.LoggerFactory
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import kotlin.random.Random


object BankAccountControllerTest : Spek({
    describe("BankAccountController Suite") {
        val logger = LoggerFactory.getLogger(javaClass)
        val embeddedServer: EmbeddedServer = ApplicationContext.run(EmbeddedServer::class.java)
        val client: HttpClient = HttpClient.create(embeddedServer.url)
        var account1Number = ""
        var account2Number = ""

        it("should create an account with /bank-accounts url - account1") {
            val request = HttpRequest.POST("/bank-accounts", CreateBankAccountPayload("test1", Currency.GBP))
            val response = client.toBlocking().exchange(request, BankAccountResponsePayload::class.java)
            assertEquals(HttpStatus.CREATED, response.status)
            account1Number = response.body()!!.accountNumber
            assertEquals("/bank-accounts/$account1Number", response.header("Location"))
            assertEquals(0.0, response.body()!!.balance.amount)
        }

        it("should create an account with /bank-accounts url - account2") {
            val request = HttpRequest.POST("/bank-accounts", CreateBankAccountPayload("test1", Currency.GBP))
            val response = client.toBlocking().exchange(request, BankAccountResponsePayload::class.java)
            assertEquals(HttpStatus.CREATED, response.status)
            account2Number = response.body()!!.accountNumber
            assertEquals("/bank-accounts/$account2Number", response.header("Location"))
            assertEquals(0.0, response.body()!!.balance.amount)
        }

        it("should get bank account details with /bank-accounts/{account-number} url") {
            val request = HttpRequest.GET<Any>("/bank-accounts/$account1Number")
            val response = client.toBlocking().exchange(request, BankAccountResponsePayload::class.java)
            assertEquals(HttpStatus.OK, response.status)
            assertEquals(account1Number, response.body()!!.accountNumber)
            assertEquals(0.0, response.body()!!.balance.amount)
        }

        it("should list bank accountdetails with /bank-accounts url") {
            val request = HttpRequest.GET<Any>("/bank-accounts")
            val response = client.toBlocking().exchange(request, Argument.of(List::class.java, BankAccountResponsePayload::class.java))
            val accounts = response.body()!!
            assertEquals(HttpStatus.OK, response.status())
            assertEquals(2, accounts.size)
        }

        it("should add money to account1 with /bank-accounts/add-money url") {
            val addAmountRequest = HttpRequest.POST("/bank-accounts/add-amount",
                    AddAmountPayload(account1Number, MoneyPayload(Money.gbp(1500.0))))
            val response = client.toBlocking().exchange(addAmountRequest, BankAccountResponsePayload::class.java)
            assertEquals(HttpStatus.OK, response.status)
            assertEquals(1500.0, response.body()!!.balance.amount)
        }

        it("should add money to account2 with /bank-accounts/add-money url") {
            val addAmountRequest = HttpRequest.POST("/bank-accounts/add-amount",
                    AddAmountPayload(account2Number, MoneyPayload(Money.gbp(990.92))))
            val response = client.toBlocking().exchange(addAmountRequest, BankAccountResponsePayload::class.java)
            assertEquals(HttpStatus.OK, response.status)
            assertEquals(990.92, response.body()!!.balance.amount)
        }

        it("should transfer 10.5 GBP from account1 to account2 with /bank-accounts/transfer-money url") {
            val transferMoneyRequest = HttpRequest.POST("/bank-accounts/transfer-money",
                    TransferMoneyPayload(account1Number, account2Number, MoneyPayload(Money.gbp(10.5))))
            val response = client.toBlocking().exchange(transferMoneyRequest, BankAccountResponsePayload::class.java)
            assertEquals(HttpStatus.OK, response.status)

            val bankAccountsResponse = client.toBlocking().exchange(HttpRequest.GET<Any>("/bank-accounts"), Argument.of(List::class.java, BankAccountResponsePayload::class.java))
            val account1Balance = (bankAccountsResponse.body()!!.first { (it as BankAccountResponsePayload).accountNumber == account1Number } as BankAccountResponsePayload).balance
            val account2Balance = (bankAccountsResponse.body()!!.first { (it as BankAccountResponsePayload).accountNumber == account2Number } as BankAccountResponsePayload).balance
            assertEquals(1489.5, account1Balance.amount)
            assertEquals(1001.42, account2Balance.amount)
        }

        it("should transfer money between account1 and account2 concurrently") {
            val transferFromAccount1ToAccount2 = HttpRequest.POST("/bank-accounts/transfer-money",
                    TransferMoneyPayload(account1Number, account2Number, MoneyPayload(Money.gbp(1.5))))
            val transferFromAccount2ToAccount1 = HttpRequest.POST("/bank-accounts/transfer-money",
                    TransferMoneyPayload(account2Number, account1Number, MoneyPayload(Money.gbp(1.5))))

            val transferFromAcc1 = Runnable {
                Thread.sleep(Random.nextLong(1, 90))
                client.toBlocking().exchange(transferFromAccount1ToAccount2, BankAccountResponsePayload::class.java)
                logger.info("Money Transfer From Account1 to Account2")
            }
            val transferFromAcc2 = Runnable {
                Thread.sleep(Random.nextLong(1, 15))
                client.toBlocking().exchange(transferFromAccount2ToAccount1, BankAccountResponsePayload::class.java)
                logger.info("Money Transfer From Account2 to Account1")
            }


            val threadPoolExecutor = Executors.newFixedThreadPool(20)
            val callables = mutableListOf<Callable<Any>>()
            repeat(15) { callables.add(Executors.callable(transferFromAcc1)) }
            repeat(28) { callables.add(Executors.callable(transferFromAcc2)) }

            threadPoolExecutor.invokeAll(callables)
            threadPoolExecutor.shutdown()

            val bankAccountsResponse = client.toBlocking().exchange(HttpRequest.GET<Any>("/bank-accounts"), Argument.of(List::class.java, BankAccountResponsePayload::class.java))
            val account1Balance = (bankAccountsResponse.body()!!.first { (it as BankAccountResponsePayload).accountNumber == account1Number } as BankAccountResponsePayload).balance
            val account2Balance = (bankAccountsResponse.body()!!.first { (it as BankAccountResponsePayload).accountNumber == account2Number } as BankAccountResponsePayload).balance
            assertEquals(1509.0, account1Balance.amount)
            assertEquals(981.92, account2Balance.amount)
        }

        afterGroup {
            client.close()
            embeddedServer.close()
        }
    }
})
