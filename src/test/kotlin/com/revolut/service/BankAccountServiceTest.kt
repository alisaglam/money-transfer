package com.revolut.service
import com.revolut.common.payload.MoneyPayload
import com.revolut.controller.payload.AddAmountPayload
import com.revolut.controller.payload.CreateBankAccountPayload
import com.revolut.controller.payload.TransferMoneyPayload
import com.revolut.entity.Account
import com.revolut.entity.Currency
import com.revolut.entity.Money
import com.revolut.repository.InMemoryAccountRepository
import com.revolut.service.exception.AccountCannotBeFoundException
import com.revolut.service.exception.AccountCurrencyMismatchException
import com.revolut.service.exception.InsufficientBalanceExcetion
import com.revolut.service.exception.InvalidTransferAmountException
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertFailsWith

object BankAccountServiceTest: Spek({
    describe("BankAccountService Suite") {
        var repository = mockk<InMemoryAccountRepository>(relaxed = true)
        var service = BankAccountService(repository)

        beforeEachTest {
            repository = mockk(relaxed = true)
            service = BankAccountService(repository)
        }

        context("Create Account") {
            it("should create account") {
                service.createAccount(CreateBankAccountPayload("test acc holder", Currency.EUR))

                val accountSlot = slot<Account>()
                verify(exactly = 1) { repository.save(capture(accountSlot)) }

                assertEquals("test acc holder", accountSlot.captured.accountHolder)
                assertEquals(Currency.EUR, accountSlot.captured.currency)
                assertEquals(Money.createMoney(0.0, Currency.EUR), accountSlot.captured.balance)
            }
        }

        context("Add Amount To Account") {
            it("should add amount to account") {
                every { repository.getByAccountNumber("acc1") } returns Account("acc1", "test", Currency.GBP, Money.gbp(10.0))
                service.addAmountToAccount(AddAmountPayload("acc1", MoneyPayload(Money.gbp(100.0))))

                val accountSlot = slot<Account>()
                verify(exactly = 1) { repository.save(capture(accountSlot)) }

                assertEquals(Money.createMoney(110.0, Currency.GBP), accountSlot.captured.balance)
            }

            it("should give exception as account number not existing while adding money to the account") {
                every { repository.getByAccountNumber("acc1") } returns null
                assertFailsWith(AccountCannotBeFoundException::class) {
                    service.addAmountToAccount(AddAmountPayload("acc1", MoneyPayload(Money.gbp(100.0))))
                }
            }

            it("should give exception as the currency of added amount is not matching to the currency of the account") {
                every { repository.getByAccountNumber("acc1") } returns Account("acc1", "test", Currency.GBP, Money.gbp(10.0))
                assertFailsWith(AccountCurrencyMismatchException::class) {
                    service.addAmountToAccount(AddAmountPayload("acc1", MoneyPayload(Money.usd(100.0))))
                }
            }

            it("should give exception since it is not allowed to add zero amount") {
                every { repository.getByAccountNumber("acc1") } returns Account("acc1", "test", Currency.GBP, Money.gbp(10.0))
                assertFailsWith(InvalidTransferAmountException::class) {
                    service.addAmountToAccount(AddAmountPayload("acc1", MoneyPayload(Money.gbp(0.0))))
                }
            }
        }

        context("Transfer Money") {
            it("should transfer money") {
                val account1 = Account("acc1", "test", Currency.GBP, Money.gbp(13.0))
                every { repository.getByAccountNumber("acc1") } returns account1
                val account2 = Account("acc2", "test", Currency.GBP, Money.gbp(10.0))
                every { repository.getByAccountNumber("acc2") } returns account2
                service.transferMoney(TransferMoneyPayload("acc1", "acc2", MoneyPayload(Money.gbp(12.25))))

                verify(exactly = 1) { repository.save(account1.copy(balance = Money.gbp(0.75))) }
                verify(exactly = 1) { repository.save(account2.copy(balance = Money.gbp(22.25))) }
            }

            it("should give error as acc1 is not an existing account number") {
                every { repository.getByAccountNumber("acc1") } returns null
                val account2 = Account("acc2", "test", Currency.GBP, Money.gbp(10.0))
                every { repository.getByAccountNumber("acc2") } returns account2
                assertFailsWith(AccountCannotBeFoundException::class) {
                    service.transferMoney(TransferMoneyPayload("acc1", "acc2", MoneyPayload(Money.gbp(12.25))))
                }
            }

            it("should give error as acc2 is not an existing account number") {
                val account1 = Account("acc1", "test", Currency.GBP, Money.gbp(13.0))
                every { repository.getByAccountNumber("acc1") } returns account1
                every { repository.getByAccountNumber("acc2") } returns null
                assertFailsWith(AccountCannotBeFoundException::class) {
                    service.transferMoney(TransferMoneyPayload("acc1", "acc2", MoneyPayload(Money.gbp(12.25))))
                }
            }

            it("should give error as currency of source account is not matching to the currency of transferred amount") {
                val account1 = Account("acc1", "test", Currency.USD, Money.usd(13.0))
                every { repository.getByAccountNumber("acc1") } returns account1
                val account2 = Account("acc2", "test", Currency.GBP, Money.gbp(10.0))
                every { repository.getByAccountNumber("acc2") } returns account2
                assertFailsWith(AccountCurrencyMismatchException::class) {
                    service.transferMoney(TransferMoneyPayload("acc1", "acc2", MoneyPayload(Money.gbp(12.25))))
                }
            }

            it("should give error as currency of target account is not matching to the currency of transferred amount") {
                val account1 = Account("acc1", "test", Currency.GBP, Money.gbp(13.0))
                every { repository.getByAccountNumber("acc1") } returns account1
                val account2 = Account("acc2", "test", Currency.USD, Money.usd(10.0))
                every { repository.getByAccountNumber("acc2") } returns account2
                assertFailsWith(AccountCurrencyMismatchException::class) {
                    service.transferMoney(TransferMoneyPayload("acc1", "acc2", MoneyPayload(Money.gbp(12.25))))
                }
            }

            it("should give error as transferred amount is zero") {
                val account1 = Account("acc1", "test", Currency.GBP, Money.gbp(13.0))
                every { repository.getByAccountNumber("acc1") } returns account1
                val account2 = Account("acc2", "test", Currency.GBP, Money.gbp(10.0))
                every { repository.getByAccountNumber("acc2") } returns account2
                assertFailsWith(InvalidTransferAmountException::class) {
                    service.transferMoney(TransferMoneyPayload("acc1", "acc2", MoneyPayload(Money.gbp(0.0))))
                }
            }

            it("should give error as transferred amount is greater than the amount source account has") {
                val account1 = Account("acc1", "test", Currency.GBP, Money.gbp(13.0))
                every { repository.getByAccountNumber("acc1") } returns account1
                val account2 = Account("acc2", "test", Currency.GBP, Money.gbp(10.0))
                every { repository.getByAccountNumber("acc2") } returns account2
                assertFailsWith(InsufficientBalanceExcetion::class) {
                    service.transferMoney(TransferMoneyPayload("acc1", "acc2", MoneyPayload(Money.gbp(13.10))))
                }
            }
        }
    }
})
