# Money-Transfer Assignment

In this project, RESTful api endpoints are implemented for bank account operations

## Technology

It is written with `Kotlin`. Because of the limitation, i haven't chosen
Spring as framework here. I have searched for other lightweight frameworks and i found `Micronaut`
which works well with Kotlin. I am not sure if it is such a lightweight framework but in this assignment
i wanted to try a framework which i didn't work before. Basically it contains an embedded servlet container 
(Netty by default) and a provides support for developing RESTful services. It is a modular framework which means
you can increase its capability by adding its existing modules (e.g. micronaut-kafka) to your project if you need

`Micronaut` has a CLI and I used it to create the project first. It has created a gradle based project.
It also offers `Spek` as testing framework. 

## Design

This project has few endpoints and all are simple synchronous request/response services.
Although only one endpoint (transfer-money) is expected on the assignment, i have implemented some others in order to 
be able to demonstrate the transfer-money api is working well.

 
I have used layered architecture approach here. We have `controller ` classes which defines the rest endpoints, 
`service` classes where we implement business logic and `repository` for persistence layer. In this project i have used 
Map data structure inside in memory repository. 

On service layer i have checked some rules while executing the operations and 
use repository to persist updated data. Exception mechanism is used when there is something 
which is not applicable. These exceptions are converted to localized user messages by using 
exception handler mechanism of micronaut. Another thing that we handled on service layer is concurrency. 
Because these endpoints might be used by multiple systems and they might want to update
same accounts at the same time we handled concurrency here. 

I have decided to create a Money object and encapsulate money related methods in it. In order to enable apis to work with multiple currencies 
i have added currency logic here.

We don't keep track of money transfers or other money flows. I assumed that it is not mandatory for this 
project. But, ideally we should keep track of all those kind of operations. 

## Testing

We have `BankAccountControllerTest` for demonstrating that the api is working well. This test contains test cases which 
adds accounts, adda amounts to these accounts and makes transfer between the accounts. We have a seperate 
test case for simultaneously making transfer between bank accounts.

## How to run 

###### Dependencies
- Java

In order to execute test the following command needs to be executed

```./gradlew test```

In the project directory you can execute following command which will run the server on 8080 port.

```./gradlew run```

If you want to the stop server, you can execute the commond:

```./gradlew --stop```

## Code Comment
I don't think we need code comment for each method here
